import { EdenClientPage } from './app.po';

describe('eden-client App', function() {
  let page: EdenClientPage;

  beforeEach(() => {
    page = new EdenClientPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
