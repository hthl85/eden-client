import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

import { AppComponent } from './app.component';

// Import app pages
import { SignInModule } from './pages/sign-in/sign-in.module';
import { SignUpModule } from './pages/sign-up/sign-up.module';
import { HomeModule } from './pages/home/home.module';
import { DashboardModule } from './pages/dashboard/dashboard.module';
import { DeviceModule } from './pages/device/device.module';

// Import app services
import { routes } from './app.routes';
import { UserService } from './services/user.service';
import { DeviceService } from './services/device.service';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    RouterModule.forRoot(routes),
    // App pages
    SignInModule,
    SignUpModule,
    HomeModule,
    DashboardModule,
    DeviceModule
  ],
  providers: [
    UserService,
    DeviceService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
