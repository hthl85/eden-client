import { Routes } from '@angular/router';

import { HomeRoutes } from './pages/home/home.routes';
import { SignInRoutes } from './pages/sign-in/sign-in.routes';
import { SignUpRoutes } from './pages/sign-up/sign-up.routes';

export const routes: Routes = [
  ...SignInRoutes,
  ...SignUpRoutes,
  ...HomeRoutes
];
