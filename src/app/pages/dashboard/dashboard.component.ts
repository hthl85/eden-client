import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(
    private userService: UserService
  ) {
    // Grab current user info
    if (!this.userService.currUser) {
      this.userService.whoAmI().subscribe((user) => {
        this.userService.currUser = user;
      });
    }
  }

  ngOnInit() { }

}
