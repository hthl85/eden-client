import { Route } from '@angular/router';
import { DeviceComponent } from './device.component';

export const DeviceRoutes: Route[] = [
  {
    path: 'device',
    component: DeviceComponent
  }
];
