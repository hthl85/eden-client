import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private router: Router, private userService: UserService) { }

  ngOnInit() {
  }

  /**
   * Handle the sign out event
   * (sign out and navigate to signin page)
   */
  onSignOut(): void {
    this.userService.signOut().subscribe((data) => {
      if (!data.loggedOut) {
        this.router.navigate(['/signin']);
      }
    });
  }

}
