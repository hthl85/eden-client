import { Route } from '@angular/router';

import { AuthenticatedGuard } from '../../shared/guard/authenticated.guard';
import { HomeComponent } from './home.component';
import { DashboardRoutes } from '../dashboard/dashboard.routes';
import { DeviceRoutes } from '../device/device.routes';

export const HomeRoutes: Route[] = [
  {
    path: '',
    component: HomeComponent,
    canActivate: [AuthenticatedGuard],
    children: [
      ...DashboardRoutes,
      ...DeviceRoutes
    ]
  }
];
