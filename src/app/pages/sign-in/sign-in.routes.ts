import { Route } from '@angular/router';
import { SignInComponent } from './sign-in.component';
import { UnauthenticatedGuard } from '../../shared/guard/unauthenticated.guard';

export const SignInRoutes: Route[] = [
  {
    path: 'signin',
    component: SignInComponent,
    canActivate: [UnauthenticatedGuard]
  }
];
