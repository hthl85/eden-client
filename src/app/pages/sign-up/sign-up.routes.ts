import { Route } from '@angular/router';
import { SignUpComponent } from './sign-up.component';
import { UnauthenticatedGuard } from '../../shared/guard/unauthenticated.guard';

export const SignUpRoutes: Route[] = [
  {
    path: 'signup',
    component: SignUpComponent,
    canActivate: [UnauthenticatedGuard]
  }
];
