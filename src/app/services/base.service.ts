// Import observable and its operator
import { Observable } from 'rxjs/Observable';

// Import shared utilities
import { EnvConfig } from '../shared/index';

export class BaseService {

  conf: EnvConfig;

  constructor() {
    this.conf = new EnvConfig();
  }

  protected handleError(error: any): Observable<any> {
    // in a real world app, we may send the server to some remote logging infrastructure
    // instead of just logging it to the console
    let err: Error;
    if (error && error._body) {
      err = JSON.parse(error._body);
    } else {
      err = new Error('Server Error');
    }
    return Observable.throw(err);
  }

}
