import { Injectable } from '@angular/core';
import {
  Http,
  Headers,
  Response,
  RequestOptionsArgs
} from '@angular/http';

// Import observable and its operator
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

// Import socket io
import * as io from 'socket.io-client';

import { BaseService } from './base.service';
import { Device, DeviceList } from '../shared/index';

@Injectable()
export class DeviceService extends BaseService {

  private socket;
  private url = 'http://localhost:3000';

  // Observable subject
  private insertStatusSubject = new Subject<boolean>();
  private selectDeviceSubject = new Subject<Device>();

  // Observable status streams
  insertStatusObserver = this.insertStatusSubject.asObservable();
  selectDeviceObserver = this.selectDeviceSubject.asObservable();

  constructor(private http: Http) {
    super();
    this.socket = io(this.url);
  }

  /**
   * Handle add new device action
   */
  addDevice(device: Device): Observable<any> {
    let headers = new Headers();
    let body = JSON.stringify(device);
    let url = this.conf.getUrlByAction('addDevice');

    // Set header
    headers.append('Content-Type', 'application/json');

    return this.http.post(url, body, <RequestOptionsArgs>{ headers: headers, withCredentials: true })
      .map((res: Response) => {
        console.log('DeviceService - addDevice success');
        console.log(res);

        // Announce add new device success to all subscriber
        this.insertStatusSubject.next(true);

        return res.json();
      })
      .catch(this.handleError);
  }

  selectDatum(device: Device) {
    console.log('selectDatum is called');
    console.log(device);
    this.selectDeviceSubject.next(device);
  }

  getData(limit: number, last: string): Observable<DeviceList> {
    let headers = new Headers();
    let url = this.conf.getUrlByActionWithParams('getDevices', [limit.toString(), last]);

    // Set header
    headers.append('Content-Type', 'application/json');

    return this.http.get(url, <RequestOptionsArgs>{ headers: headers, withCredentials: true })
      .map((res: Response) => {
        console.log('DeviceService - getDevices success');
        console.log(res);
        let jsonObj = <DeviceList>res.json();
        return new DeviceList(jsonObj.total, jsonObj.data);
      })
      .catch(this.handleError);
  }

  init(clientInfo) {
    console.log('init device socket');
    console.log(clientInfo);
    this.socket.emit('initConnection', clientInfo);
  }

  destroy() {
    console.log('destroy device socket');
    this.socket.emit('destroyConnection');
  }

  togglePin(pinStates: any) {
    console.log('togglePin device socket');
    this.socket.emit('togglePin', pinStates);
  }

}
