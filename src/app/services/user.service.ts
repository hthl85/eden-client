import { Injectable } from '@angular/core';
import {
  Http,
  Headers,
  Response,
  RequestOptionsArgs
} from '@angular/http';

// Import observable and its operator
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import 'rxjs/add/observable/of';
import 'rxjs/add/observable/throw';

import { BaseService } from './base.service';
import { User } from '../shared/index';

@Injectable()
export class UserService extends BaseService {

  public currUser: any;

  constructor(private http: Http) {
    super();
    this.currUser = null;
  }

  /**
   * Ensure request is authenticated
   */
  isAuth(): Observable<any> {
    let url = this.conf.getUrlByAction('isAuth');

    return this.http.get(url, <RequestOptionsArgs>{ withCredentials: true })
      .map((res: Response) => {
        console.log('UserService - isAuth success');
        console.log(res);
        return res.json();
      })
      .catch((error: Response) => {
        if (error.status === 401) {
          console.log('UserService - isAuth unsuccess');
          return Observable.of(error);
        }
        return this.handleError(error);
      });
  }

  /**
   * Handle sign in action
   */
  signIn(user: User): Observable<any> {
    let headers = new Headers();
    let body = JSON.stringify(user);
    let url = this.conf.getUrlByAction('signIn');

    // Set header
    headers.append('Content-Type', 'application/json');

    return this.http.post(url, body, <RequestOptionsArgs>{ headers: headers, withCredentials: true })
      .map((res: Response) => {
        console.log('UserService - signIn success');
        console.log(res);
        return res.json();
      })
      .catch(this.handleError);
  }

  /**
   * Handle sign up action
   */
  signUp(user: User): Observable<any> {
    let headers = new Headers();
    let body = JSON.stringify(user);
    let url = this.conf.getUrlByAction('signUp');

    // Set header
    headers.append('Content-Type', 'application/json');

    return this.http.post(url, body, { headers: headers })
      .map((res: Response) => {
        console.log('UserService - signUp success');
        console.log(res);
        return res.json();
      })
      .catch(this.handleError);
  }

  /**
   * Handle sign out action
   */
  signOut(): Observable<any> {
    let url = this.conf.getUrlByAction('signOut');

    return this.http.get(url, <RequestOptionsArgs>{ withCredentials: true })
      .map((res: Response) => {
        console.log('UserService - signOut success');
        console.log(res);
        return res;
      })
      .catch(this.handleError);
  }

  /**
   * Get current user info
   */
  whoAmI(): Observable<any> {
    let url = this.conf.getUrlByAction('whoAmI');

    return this.http.get(url, <RequestOptionsArgs>{ withCredentials: true })
      .map((res: Response) => {
        console.log('UserService - whoAmI success');
        console.log(res);
        return res.json();
      })
      .catch(this.handleError);
  }

}
