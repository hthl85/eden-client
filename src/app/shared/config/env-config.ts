import { environment } from '../../../environments/environment';

export class EnvConfig {
  constructor() { }

  getUrlByAction(action: string): string {
    let url: string[] = [];

    url.push(environment.api);
    url.push(environment.routes[action]);

    return url.join('/');
  }

  getUrlByActionWithParams(action: string, params: string[]): string {
    let url: string[] = [];

    url.push(environment.api);
    url.push(environment.routes[action].path);

    params.forEach((p, idx) => {
      url.push(environment.routes[action].params[idx]);
      url.push(params[idx]);
    });

    return url.join('/');
  }
}
