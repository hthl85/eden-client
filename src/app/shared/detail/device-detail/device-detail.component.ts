import { Component, OnInit, OnDestroy } from '@angular/core';

import { Subscription } from 'rxjs/Subscription';

import { UserService } from '../../../services/user.service';
import { DeviceService } from '../../../services/device.service';
import { Device } from '../../index';

@Component({
  selector: 'app-device-detail',
  templateUrl: './device-detail.component.html',
  styleUrls: ['./device-detail.component.css']
})
export class DeviceDetailComponent implements OnInit, OnDestroy {
  private currUser: any;
  public selectedDevice: Device;

  constructor(
    private userService: UserService,
    private deviceService: DeviceService
  ) {
    // if (!this.userService.currUser) {
    //   console.log('Current user is not available. Make API call.');
    //   this.userService.whoAmI().subscribe((userInfo) => {
    //     this.currUser = userInfo;
    //     this.userService.currUser = userInfo;
    //   });
    // } else {
    //   console.log('Current user is available. Ok.');
    //   this.currUser = this.userService.currUser;
    // }
    this.selectedDevice = null;
  }

  ngOnInit() {
    let subscription: Subscription;
    subscription = this.deviceService.selectDeviceObserver.subscribe((device: Device) => {
      this.selectedDevice = new Device(device);
      console.log('selected Device');
      console.log(this.selectedDevice);
      // console.log('current user');
      // console.log(this.userService.currUser);
      // this.selectedDevice['uuid'] = this.currUser.uuid;
      // this.deviceService.init({
      //   uuid: this.currUser.uuid,
      //   deviceId: this.selectedDevice.id,
      //   secret: this.selectedDevice.getSecret()
      // });
      this.deviceService.init(this.selectedDevice);
    });
  }

  ngOnDestroy() {
    this.deviceService.destroy();
  }

  onClickTest001() {
    let pinStates = {
      type: 2,
      pins: ['D1'],
      vals: [1]
    };
    this.deviceService.togglePin(pinStates);
  }

  onClickTest002() {
    let pinStates = {
      type: 2,
      pins: ['D1'],
      vals: [0]
    };
    this.deviceService.togglePin(pinStates);
  }

}
