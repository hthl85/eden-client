import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { MdSnackBar } from '@angular/material';
import { DeviceService } from '../../../services/device.service';
import { BaseForm } from '../base-form';

@Component({
  selector: 'app-add-device-form',
  templateUrl: './add-device-form.component.html',
  styleUrls: ['./add-device-form.component.css'],
  providers: [MdSnackBar]
})
export class AddDeviceFormComponent extends BaseForm implements OnInit {

  /**
   * Device Form controls
   */
  deviceForm: FormGroup;
  id: FormControl;
  name: FormControl;
  subTopic: FormControl;
  pubTopic: FormControl;
  desc: FormControl;

  constructor(private deviceService: DeviceService, snackBar: MdSnackBar) {
    super(snackBar);
  }

  ngOnInit() {
    this.initSignInForm();
  }

  /**
   * Handle event when user submits signin form (redirect to index page if success)
   */
  onSubmit() {
    // Innocent until proven guilty
    this.submitted = true;
    this.errorDiagnostic = null;

    this.deviceService.addDevice(this.deviceForm.value).subscribe((data) => {
      this.presentSnackBar('Add/Update device successfully.', 3000);
      this.changeSubmitedStatus(1000);
      this.deviceForm.reset();
    }, (error: Error) => {
      this.errorDiagnostic = error.message;
      this.changeSubmitedStatus(1000);
    });
  }

  /**
   * Initialize signin form control, group, and set up form validators
   */
  private initSignInForm() {
    let regex = /^[a-zA-Z0-9]+([-_\.][a-zA-Z0-9]+)*[a-zA-Z0-9]*$/;
    this.id = new FormControl('', Validators.compose([
      Validators.required,
      Validators.pattern(regex),
      Validators.minLength(6),
      Validators.maxLength(64)
    ]));

    this.name = new FormControl('', Validators.compose([
      Validators.required,
      Validators.minLength(6),
      Validators.maxLength(64)
    ]));

    this.subTopic = new FormControl('', Validators.compose([
      Validators.required,
      Validators.pattern(regex),
      Validators.minLength(6),
      Validators.maxLength(64)
    ]));

    this.pubTopic = new FormControl('', Validators.compose([
      Validators.required,
      Validators.pattern(regex),
      Validators.minLength(6),
      Validators.maxLength(64)
    ]));

    this.desc = new FormControl('');

    this.deviceForm = new FormGroup({
      id: this.id,
      name: this.name,
      subTopic: this.subTopic,
      pubTopic: this.pubTopic,
      desc: this.desc
    });
  }

}
