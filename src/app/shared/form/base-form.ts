import { MdSnackBar, MdSnackBarConfig } from '@angular/material';

export class BaseForm {

  title: string;

  /**
   * Boolean used in telling the UI that the form has 
   * been submitted and is awaiting a response
   */
  submitted: boolean = false;

  /**
   * Diagnostic message from received form request error
   */
  errorDiagnostic: string;

  constructor(private snackBar: MdSnackBar) { }

  /**
   * Display snack bar
   * TODO: Enable duration when it available
   */
  presentSnackBar(msg, duration) {
    let config = new MdSnackBarConfig();
    // config.duration = duration;
    this.snackBar.open(msg, 'Okey!', config);
  }

  /**
   * Reset submited status, to prevent submit multiple times.
   */
  changeSubmitedStatus(time: number) {
    setTimeout(() => {
      this.submitted = false;
    }, time);
  }

}
