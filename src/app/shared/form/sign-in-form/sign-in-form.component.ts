import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { MdSnackBar } from '@angular/material';
import { BaseForm } from '../base-form';
import { UserService } from '../../../services/user.service';

@Component({
  selector: 'app-sign-in-form',
  templateUrl: './sign-in-form.component.html',
  styleUrls: ['./sign-in-form.component.css'],
  providers: [MdSnackBar]
})
export class SignInFormComponent extends BaseForm implements OnInit {

  /**
   * Signin Form controls
   */
  signInForm: FormGroup;
  password: FormControl;
  usernameOrEmail: FormControl;

  constructor(
    private router: Router,
    private userService: UserService,
    snackBar: MdSnackBar
  ) {
    super(snackBar);
    this.title = 'Sign In';
  }

  /**
   * Initialize the component and the signin form
   */
  ngOnInit() {
    this.initSignInForm();
  }

  /**
   * Handle event when user submits signin form (redirect to index page if success)
   */
  onSubmit() {
    // Innocent until proven guilty
    this.submitted = true;
    this.errorDiagnostic = null;

    this.userService.signIn(this.signInForm.value).subscribe((data) => {
      this.router.navigateByUrl('/');
    }, (error: Error) => {
      this.submitted = false;
      this.errorDiagnostic = error.message;
    });
  }

  /**
   * Handle event when user click sign up button (navigate to signup page)
   */
  onSignUp() {
    this.router.navigateByUrl('/signup');
  }

  /**
   * Initialize signin form control, group, and set up form validators
   */
  private initSignInForm() {
    this.usernameOrEmail = new FormControl('', Validators.compose([
      Validators.required,
      Validators.minLength(2),
      Validators.maxLength(64)
    ]));

    this.password = new FormControl('', Validators.compose([
      Validators.required,
      Validators.minLength(2),
      Validators.maxLength(32)
    ]));

    this.signInForm = new FormGroup({
      usernameOrEmail: this.usernameOrEmail,
      password: this.password
    });
  }
}
