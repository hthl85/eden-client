import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

import { MdSnackBar } from '@angular/material';
import { BaseForm } from '../base-form';
import { UserService } from '../../../services/user.service';

@Component({
    selector: 'app-sign-up-form',
    templateUrl: './sign-up-form.component.html',
    styleUrls: ['./sign-up-form.component.css'],
    providers: [MdSnackBar]
})
export class SignUpFormComponent extends BaseForm implements OnInit {

    /**
     * Signup Form controls
     */
    signUpForm: FormGroup;
    email: FormControl;
    firstName: FormControl;
    lastName: FormControl;
    password: FormControl;
    username: FormControl;

    /**
     * Creates a new FormSignUpComponent with the injected Router, UserService 
     */
    constructor(
        private router: Router,
        private userService: UserService,
        snackBar: MdSnackBar) {
        super(snackBar);
        this.title = 'Sign Up';
    }

    /**
     * Initialize the component and the signin form
     */
    ngOnInit() {
        this.initSignUpForm();
    }

    /**
     * Handle event when user submits signup form (redirect to index page if success)
     */
    onSubmit() {
        // Innocent until proven guilty
        this.submitted = true;
        this.errorDiagnostic = null;

        this.userService.signUp(this.signUpForm.value).subscribe((data) => {
            this.router.navigateByUrl('/signin');
        }, (error: Error) => {
            this.submitted = false;
            this.errorDiagnostic = error.message;
        });
    }

    /**
     * Handle event when user click sign up button (navigate to signup page)
     */
    onSignIn() {
        this.router.navigateByUrl('/signin');
    }

    /**
     * Initialize signup form control, group, and set up form validators
     */
    private initSignUpForm() {
        let emailRegex: string = '[a-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&\'*+/=?^_`{|}~-]+)*'
            + '@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?';

        this.email = new FormControl('', Validators.compose([
            Validators.required,
            Validators.pattern(emailRegex)
        ]));

        this.firstName = new FormControl('', Validators.compose([
            Validators.required,
            Validators.minLength(2),
            Validators.maxLength(64)
        ]));

        this.lastName = new FormControl('', Validators.compose([
            Validators.required,
            Validators.minLength(2),
            Validators.maxLength(64)
        ]));

        this.password = new FormControl('', Validators.compose([
            Validators.required,
            Validators.minLength(2),
            Validators.maxLength(32)
        ]));

        this.username = new FormControl('', Validators.compose([
            Validators.required,
            Validators.minLength(2),
            Validators.maxLength(64)
        ]));

        this.signUpForm = new FormGroup({
            email: this.email,
            firstName: this.firstName,
            lastName: this.lastName,
            password: this.password,
            username: this.username
        });
    }

}
