import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { UserService } from '../../services/user.service';

@Injectable()
export class AuthenticatedGuard implements CanActivate {

  constructor(private router: Router, private userService: UserService) { }

  /**
   * When users have signed in, they can access every page but not signin or signup
   * If they tried to access signin page or signup, they would be redirected to index 
   * page. If they have not already signed in and tried to access other page, they would 
   * be redirected to the signin page.
   */
  canActivate(): Observable<boolean> | boolean {
    return this.userService.isAuth()
      .map((res: any) => {
        console.log('AuthenticatedGuard - canActivate success');
        console.log(res);

        let isAuth: boolean = res.authenticated;
        if (!isAuth) {
          // If user is unauthenticated redirect to signin page, unauthenticated user
          // can only access signin and signup page by default
          this.router.navigate(['/signin']);
        }
        return isAuth;

      }).catch((error: any) => {
        console.log('AuthenticatedGuard - canActivate unsuccess');
        // TODO: Handle the case the api server down redirect to signin no mater what
        // May be there other way better, try to find out.
        this.router.navigate(['/signin']);
        return Observable.of(true);
      });
  }
}
