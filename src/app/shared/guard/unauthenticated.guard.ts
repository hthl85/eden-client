import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { UserService } from '../../services/user.service';

@Injectable()
export class UnauthenticatedGuard implements CanActivate {

  constructor(private router: Router, private userService: UserService) { }

  /**
   * When users have not signed in, they can only access signin and signup page
   * If they tried to access other pages they would be redirected to signin page.
   * If they have already signed in they would be redirected to the index page.
   */
  canActivate(): Observable<boolean> | boolean {
    return this.userService.isAuth()
      .map((res: any) => {
        console.log('UnauthenticatedGuard - canActivate success');
        console.log(res);

        let isAuth: boolean = res.authenticated;
        if (isAuth) {
          // If user is authenticated redirect to homepage, there is no point
          // for authenticated user wandering in signin or signup page.
          this.router.navigate(['/']);
        }
        return !isAuth;

      }).catch((error: any) => {
        console.log('UnauthenticatedGuard - canActivate unsuccess');
        return Observable.of(true);
      });
  }
}
