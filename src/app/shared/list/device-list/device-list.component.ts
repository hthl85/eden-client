import { Component } from '@angular/core';

import { DeviceService } from '../../../services/device.service';

@Component({
    selector: 'app-device-list',
    templateUrl: './device-list.component.html',
    styleUrls: ['./device-list.component.css']
})
export class DeviceListComponent {

    listData: any;

    constructor(private deviceService: DeviceService) {
        this.listData = {
            limit: 3,
            title: 'Devices',
            showHeader: true,
            showFooter: true,
            service: this.deviceService,
            fields: ['name', 'desc', 'subTopic', 'pubTopic']
        };
    }

}
