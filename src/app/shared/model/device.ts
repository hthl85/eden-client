import { User } from './user';

export class Device {
  name: string;
  id: string;
  subTopic: string;
  pubTopic: string;
  desc: string;
  created: Date;
  user: User;

  private _id: string;
  private secret: string;

  constructor(obj: any) {
    let keys = Object.keys(obj);
    keys.forEach((k) => {
      this[k] = obj[k];
    });
  }

  getObjectId(): string {
    return this._id;
  }

  getSecret(): string {
    return this.secret;
  }
}

export class DeviceList {
  data: Array<Device>;
  total: number;

  constructor(total: number, devices: Array<Device>) {
    this.total = total;
    this.data = [];
    devices.forEach((d) => {
      this.data.push(new Device(d));
    });
  }
}
