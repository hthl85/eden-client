export class User {
  firstName: string;
  lastName: string;
  displayName: string;
  username: string;
  email: string;
  profilePicture: string;
  lastActive: number;

  constructor(obj: any) {
    let keys = Object.keys(obj);
    keys.forEach((k) => {
      this[k] = obj[k];
    });
  }
}
