import { NgModule, ModuleWithProviders } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '@angular/material';

import { AuthenticatedGuard } from './guard/authenticated.guard';
import { UnauthenticatedGuard } from './guard/unauthenticated.guard';
import { AddDeviceFormComponent } from './form/add-device-form/add-device-form.component';
import { SignUpFormComponent } from './form/sign-up-form/sign-up-form.component';
import { SignInFormComponent } from './form/sign-in-form/sign-in-form.component';
import { DeviceListComponent } from './list/device-list/device-list.component';
import { DeviceDetailComponent } from './detail/device-detail/device-detail.component';

import { Ng2DumblistModule } from 'ng2-dumblist';

@NgModule({
    imports: [
        CommonModule,
        ReactiveFormsModule,
        MaterialModule.forRoot(),
        Ng2DumblistModule.forRoot()
    ],
    declarations: [
        AddDeviceFormComponent,
        SignUpFormComponent,
        SignInFormComponent,
        DeviceListComponent,
        DeviceDetailComponent
    ],
    exports: [
        CommonModule,
        AddDeviceFormComponent,
        SignUpFormComponent,
        SignInFormComponent,
        DeviceListComponent,
        DeviceDetailComponent
    ]
})
export class SharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: [
                AuthenticatedGuard,
                UnauthenticatedGuard
            ]
        };
    }
}
