export const environment = {
  production: true,
  api: 'http://grangle:3000/api',
  routes: {
    // User/Auth API
    isAuth: 'auth/isauth',
    signIn: 'auth/signin',
    signUp: 'auth/signup',
    signOut: 'auth/signout',
    whoAmI: 'users/me',

    // Device API
    addDevice: 'devices',
    getDevices: {
      path: 'devices',
      params: ['limit', 'pivot']
    }
  }
};
