// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.

export const environment = {
  production: false,
  api: 'http://localhost:3000/api',
  routes: {
    // User/Auth API
    isAuth: 'auth/isauth',
    signIn: 'auth/signin',
    signUp: 'auth/signup',
    signOut: 'auth/signout',
    whoAmI: 'users/me',

    // Device API
    addDevice: 'devices',
    getDevices: {
      path: 'devices',
      params: ['limit', 'pivot']
    }
  }
};
